# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-dns-lexicon
pkgver=3.14.0
pkgrel=0
pkgdesc="Manipulate DNS records on various DNS providers in a standardized/agnostic way"
url="https://github.com/AnalogJ/lexicon"
arch="noarch"
license="MIT"
depends="
	py3-beautifulsoup4
	py3-cryptography
	py3-future
	py3-requests
	py3-tldextract
	py3-yaml
	"
makedepends="py3-gpep517 py3-poetry-core"
_providerdepends="
	py3-boto3
	py3-localzone
	py3-oci
	py3-softlayer
	py3-softlayer-zeep
	py3-xmltodict
	"
checkdepends="
	py3-filelock
	py3-pytest
	py3-pytest-mock
	py3-pytest-xdist
	py3-requests-file
	py3-vcrpy
	$_providerdepends
	"
subpackages="$pkgname-pyc"
source="$pkgname-github-$pkgver.tar.gz::https://github.com/AnalogJ/lexicon/archive/refs/tags/v$pkgver.tar.gz
	importlib.patch
	"
builddir="$srcdir/lexicon-$pkgver"
options="!check" # FileNotFoundError: [Errno 2] No such file or directory: 'oci_api_key.pem'

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n $JOBS --ignore lexicon/tests/providers/test_localzone.py lexicon
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/lexicon/tests
}

sha512sums="
1a74694935993a131f6f1d10a4bf541e9efe794c5aa6a8a976b50bb8430bd7b38e587a11b5b1e40a6db6dd1de4bbc6ba8887d61a950921d29802e4b35a64d60f  py3-dns-lexicon-github-3.14.0.tar.gz
0273320a9a5bd6371efc30a24c1291c1160fb662d2015b8720d6a2ed044d562909124a20b5b7675d965888b506423f5203ac71b77f6ac2d4e52f283daa1187ac  importlib.patch
"
