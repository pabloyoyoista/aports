# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=analitza
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/"
pkgdesc="A library to add mathematical features to your program"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	eigen-dev
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/education/analitza.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/analitza-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e58f94f83703928a31a5c59dc073a8e04145e824ef3bc9651811f8b80b5057fbd4fd605fc5abce905f4f57ab2a7fe6b31bee01d4241e91bfed1de2d2d6ba0c2d  analitza-23.04.3.tar.xz
"
