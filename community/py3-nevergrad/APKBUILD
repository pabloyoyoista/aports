# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-nevergrad
pkgver=0.11.0
pkgrel=0
pkgdesc="A Python toolbox for performing gradient-free optimization"
url="https://github.com/facebookresearch/nevergrad"
arch="noarch !s390x" #py3-bayesian-optimization
license="MIT"
depends="python3 py3-bayesian-optimization py3-cma py3-numpy py3-pandas py3-typing-extensions"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-cov"
subpackages="$pkgname-pyc"
source="https://github.com/facebookresearch/nevergrad/archive/$pkgver/nevergrad-$pkgver.tar.gz"
builddir="$srcdir/nevergrad-$pkgver"
options="!check" # several test dependencies are missing | skip tests for now

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
fc7ae46a4bf46406e501a6b21bbc5e0820d6d8565ee8dadb1529143061d8f3c16e47e308c8282b7676588a3b3dc865f17078bc0f84547595c783eb14a7bd2696  nevergrad-0.11.0.tar.gz
"
