# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kde-cli-tools
pkgver=5.27.7
pkgrel=1
pkgdesc="Tools based on KDE Frameworks 5 to better interact with the system"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma/kde-cli-tools"
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0-or-later AND GPL-2.0-only AND LGPL-2.1-only"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	kdesu-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kservice-dev
	kwindowsystem-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kde-cli-tools.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

# Workaround a circular dependency https://gitlab.alpinelinux.org/alpine/aports/-/issues/11785
install_if="plasma-workspace"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/kde-cli-tools.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
df0e295b2e31338a11508c0f9bc1a5109c07e1729a4e30dd8e79800f34b27e6eb063a2b11196a8f8e653f8439360a8fb58348b0ef69a17588b40b08e53f06991  kde-cli-tools-5.27.7.tar.xz
"
