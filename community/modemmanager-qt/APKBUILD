# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=modemmanager-qt
pkgver=5.108.0
pkgrel=2
pkgdesc="Qt wrapper for ModemManager DBus API"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	modemmanager-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # requires dbus running
_repo_url="https://invent.kde.org/frameworks/modemmanager-qt.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/modemmanager-qt-$pkgver.tar.xz"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/modemmanager-qt.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bfd3c0fc402db43a48dce2688b6cb861b0cd687f66c85cefe567285c9727c3612a6cd1e232914b2588ffab385412048f9f9895d4ba3484a6b81afc4b95adc310  modemmanager-qt-5.108.0.tar.xz
"
