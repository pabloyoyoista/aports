# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gst-libav
pkgver=1.22.5
pkgrel=0
pkgdesc="GStreamer streaming media framework libav plugin"
url="https://gstreamer.freedesktop.org"
arch="all"
license="GPL-2.0-or-later LGPL-2.0-or-later"
makedepends="
	coreutils
	ffmpeg-dev
	gst-plugins-base-dev
	gstreamer-dev
	meson
	orc-dev
	"
source="https://gstreamer.freedesktop.org/src/gst-libav/gst-libav-$pkgver.tar.xz
	flaky.patch
	"
replaces="gst-libav1"

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Dpackage-origin="https://alpinelinux.org" \
		-Dpackage-name="GStreamer libav plugin (Alpine Linux)" \
		. output
	meson compile -C output
}

check() {
	meson test --print-errorlogs --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

doc() {
	default_doc
	replaces="${pkgname}1-doc"
}

sha512sums="
a71296f4bbd794816b4a083e176bead0923087555dafce26bb0a69932f28b7ae857fe991aef6b03effd91ff13857bcbb6eba7caac6a2f1c820abf996cd2ba54e  gst-libav-1.22.5.tar.xz
b6ea595d0875c22b69fb7c32ce3e03e187ce8c76ad5992fd709500507fc89e021f6f53fc951a61b5edf0312963bf7d313542a3970bbab27e335887e5db34d26b  flaky.patch
"
