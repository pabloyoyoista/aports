# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=cargo-c
pkgver=0.9.23
pkgrel=0
pkgdesc="cargo subcommand to build and install C-ABI compatibile dynamic and static libraries"
url="https://github.com/lu-zero/cargo-c"
arch="all"
license="MIT"
# nghttp2-sys doesn't support system
makedepends="
	cargo
	cargo-auditable
	curl-dev
	libgit2-dev
	libssh2-dev
	openssl-dev>3
	zlib-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/lu-zero/cargo-c/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-Cargo.lock::https://github.com/lu-zero/cargo-c/releases/download/v$pkgver/Cargo.lock"
options="net" # To download crates

export LIBSSH2_SYS_USE_PKG_CONFIG=1
export DEP_NGHTTP2_ROOT=/usr

prepare() {
	default_prepare

	cp "$srcdir"/$pkgname-$pkgver-Cargo.lock Cargo.lock

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	rm target/release/cargo-*.d
	install -Dm755 target/release/cargo-* -t "$pkgdir"/usr/bin/

	install -Dm644 -t "$pkgdir/usr/share/doc/cargo-c" README.md
}

sha512sums="
cd93e88dc1d88c1e4911dd78f7af528b1c7fed259557d7d84d14e7a42eb7c077210386523f6d012b8de8714193956a7746684c2547b45ef03a2cff7d9be75e11  cargo-c-0.9.23.tar.gz
e0a96023ca9f72049f9b026038f00cf15da060616e6ab36f6d4e34b2cf17dfe02efa64f7c54330bf0a11cb94774d19e6d3c947be4d01640c7131285166e9d1f1  cargo-c-0.9.23-Cargo.lock
"
