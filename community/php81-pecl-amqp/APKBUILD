# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-amqp
_extname=amqp
pkgver=2.0.0_beta2
_pkgver=${pkgver/_/}
pkgrel=0
pkgdesc="PHP 8.1 extension to communicate with any AMQP spec 0-9-1 compatible server - PECL"
url="https://pecl.php.net/package/amqp"
arch="all"
license="PHP-3.01"
_phpv=81
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev rabbitmq-c-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz"
builddir="$srcdir/$_extname-$_pkgver"

install_if="php-$_extname php$_phpv"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Tests require running AMQP server, so basic check
	$_php -d extension="$builddir"/modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/40_$_extname.ini
}

sha512sums="
6decad8d9d96402e64f75e988f479dd40784709c8b328067647d7a07f0e4cc53d9b100a3058d982fadb4aab12549d25f3507b54e6dd1dd6bd2b28755948c8ee8  php-pecl-amqp-2.0.0_beta2.tgz
"
