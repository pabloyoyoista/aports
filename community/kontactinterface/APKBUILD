# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kontactinterface
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
pkgdesc="Kontact Plugin Interface Library"
license="LGPL-2.0-only OR LGPL-3.0-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kparts-dev
	kwindowsystem-dev
	kxmlgui-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/pim/kontactinterface.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontactinterface-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
24b29fd1b7542e2dee53799aa739ac01c916dd714027345181a056cee556cb2fe76cb1ddf9776ab9d88cb8daf0e038354f14fc863193f66c0d5331e427792397  kontactinterface-23.04.3.tar.xz
"
