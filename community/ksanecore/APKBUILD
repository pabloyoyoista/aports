# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ksanecore
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/graphics/"
pkgdesc="Library providing logic to interface scanners"
license="BSD-2-Clause AND BSD-3-Clause AND CC0-1.0 AND (LGPL-2.1-only OR LGPL-3.0-only) AND LicenseRef-KDE-Accepted-LGPL"
depends_dev="
	ki18n-dev
	qt5-qtbase-dev
	sane-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/libraries/ksanecore.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksanecore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6a063e2038e3f34e8b0739678c1d24cea4f39115c51546cd79f27f13e5829157b46bdd6889bf713a6bf89b8ff918b79bcfaa9ae4b782cdb30a6b865443280b27  ksanecore-23.04.3.tar.xz
"
