# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=greenbone-feed-sync
# follow the same version of gvm-tools
pkgver=23.7.0
pkgrel=0
pkgdesc="New script for syncing the Greenbone Community Feed"
url="https://github.com/greenbone/greenbone-feed-sync"
arch="noarch !armhf !ppc64le" # limited by py3-pytest-httpx
license="GPL-3.0-or-later"
depends="
	py3-rich
	python3
	rsync
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-poetry-core
	py3-lxml
	"
checkdepends="
	py3-pontos
	py3-pytest
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/greenbone/greenbone-feed-sync/archive/refs/tags/v$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
b2357e400e1b8fccf64815ef80fb9800d06260d946a2aca946c887101a90833407c54f6e9b022fbb418fb08bfd5bc4a007a1b340f6deed143bb822ed1118fc64  greenbone-feed-sync-23.7.0.tar.gz
"
