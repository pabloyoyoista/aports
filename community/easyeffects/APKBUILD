# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=easyeffects
pkgver=7.0.6
pkgrel=0
pkgdesc="audio plugins for PipeWire applications"
url="https://github.com/wwmm/easyeffects"
# s390x: blocked by pipewire
arch="all !s390x"
license="GPL-3.0-or-later"
install="$pkgname.post-install"
depends="lv2"
makedepends="
	appstream-glib-dev
	desktop-file-utils
	fftw-dev
	fmt-dev
	gsl-dev
	gtk4.0-dev
	itstool
	libadwaita-dev
	libbs2b-dev
	libebur128-dev
	libsamplerate-dev
	libsigc++3-dev
	libsndfile-dev
	lilv-dev
	lv2-dev
	meson
	nlohmann-json
	onetbb-dev
	pipewire-dev
	rnnoise-dev
	soundtouch-dev
	speexdsp-dev
	zita-convolver-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
source="$pkgname-$pkgver.tar.gz::https://github.com/wwmm/easyeffects/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # no testsuite

build() {
	abuild-meson . build
	meson compile -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
}

sha512sums="
8fb94551e9f434b6695412bc60ccb86f5d5b56b043b17ae3509c909000883c2e853090ca79370db93b27ab6d8077a248c491ecd5415308dc55d055e5b1388f40  easyeffects-7.0.6.tar.gz
"
