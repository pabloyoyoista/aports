# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kwidgetsaddons
pkgver=5.108.0
pkgrel=2
pkgdesc="Addons to QtWidgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-only AND LGPL-2.1-only AND Unicode-DFS-2016"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="
	mesa-dri-gallium
	xvfb-run
	"
_repo_url="https://invent.kde.org/frameworks/kwidgetsaddons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwidgetsaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kwidgetsaddons.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# ktwofingertaptest, ktwofingerswipetest and and ksqueezedtextlabelautotest are broken
	xvfb-run ctest --test-dir build --output-on-failure -E '(ktooltipwidget|ktwofingertap|ktwofingerswipe|ksqueezedtextlabelauto)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8425bac66b8927e5276dcf2ff0c6228c28a7349b4ee203f574d2c54c437c8aa6e703f88b29c430c05e5a372044e3c60571c6df314e0d424270047277e2c543d3  kwidgetsaddons-5.108.0.tar.xz
"
