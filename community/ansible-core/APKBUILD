# Contributor: Sean McAvoy <seanmcavoy@gmail.com>
# Maintainer: Sean McAvoy <seanmcavoy@gmail.com>
pkgname=ansible-core
pkgver=2.15.2
pkgrel=0
pkgdesc="core components of ansible: A configuration-management, deployment, task-execution, and multinode orchestration framework"
url="https://ansible.com"
options="!check" # for now
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-cryptography
	py3-jinja2
	py3-packaging
	py3-paramiko
	py3-resolvelib
	py3-yaml
	python3
	"
makedepends="py3-setuptools"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://pypi.python.org/packages/source/a/ansible-core/ansible-core-$pkgver.tar.gz
	"

replaces="ansible-base"

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/examples/
	cp -r examples/* \
	"$pkgdir"/usr/share/doc/$pkgname/examples/
	install -m644 README.rst "$pkgdir"/usr/share/doc/$pkgname

	mkdir -p "$pkgdir"/usr/share/man/
	local man
	for man in ./docs/man/man?/*.?; do
		install -Dm644 "$man" \
			"$pkgdir"/usr/share/man/man${man##*.}/${man##*/}
	done
}

sha512sums="
75cfc9a5f104d15a83ea464e34b9b2d351fcddc23b7bc4e86a1975d2aef22a336c865a4bd25cee58ba64b67eebfc2553ca55ea581ffcfc4e5261af46fbdf7e55  ansible-core-2.15.2.tar.gz
"
