# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=skopeo
pkgver=1.13.2
pkgrel=0
# set this to the gitrev of the version
_gitrev=24c0c3369d8e6c3d4dd60de8c747b89b270d7f12
pkgdesc="Work with remote images registries - retrieving information, images, signing content"
url="https://github.com/containers/skopeo"
license="Apache-2.0"
arch="all"
options="net !check" # needs docker
depends="containers-common"
makedepends="
	bash
	btrfs-progs-dev
	glib-dev
	go
	go-md2man
	gpgme-dev
	libselinux-dev
	linux-headers
	lvm2-dev
	ostree-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/containers/skopeo/archive/v$pkgver/skopeo-$pkgver.tar.gz"

# secfixes:
#   1.5.2-r0:
#     - CVE-2021-41190

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make GIT_COMMIT=$_gitrev all
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install-docs install-completions
	install -Dm755 bin/skopeo -t "$pkgdir"/usr/bin/
}

sha512sums="
8d321fba452784e3704c058113eb29c76a26e71aa19d0ba2c48fa077e0b74a64a1b8c4ba121d35a1d9ba886a5c28f2e66891e3f592654f2f738d8659443179b3  skopeo-1.13.2.tar.gz
"
