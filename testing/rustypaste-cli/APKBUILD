# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=rustypaste-cli
pkgver=0.7.0
pkgrel=0
pkgdesc="CLI tool for rustypaste"
url="https://github.com/orhun/rustypaste-cli"
# s390x, ppc64le, riscv64: blocked by ring crate
arch="all !s390x !ppc64le !riscv64"
license="MIT"
makedepends="cargo cargo-auditable"
subpackages="$pkgname-doc"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/rustypaste-cli/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features use-native-certs
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 "target/release/rpaste" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

sha512sums="
b7298e07ba8514922aacebeb9c922e6477bbf65d8395606da375b79297373bb9942ceb8c56eab76677ed0838cc0960088435906e21ab2b79d8a8003915f6ccd6  rustypaste-cli-0.7.0.tar.gz
"
