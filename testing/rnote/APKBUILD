# Contributor: Jakob Meier <comcloudway@ccw.icu>
# Maintainer: Jakob Meier <comcloudway@ccw.icu>
pkgname=rnote
pkgver=0.7.1
pkgrel=1
pkgdesc="Sketch and take handwritten notes."
url="https://rnote.flxzt.net/"
# ppc64le: vendored abseil, not possible to override to system
# armhf: doesn't build
# s390x: mainframe
arch="all !armhf !s390x !ppc64le"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	appstream-glib-dev
	appstream-dev
	cargo
	clang-dev
	cmake
	desktop-file-utils
	gtk4.0-dev
	libadwaita-dev
	meson
	poppler-dev
	"
subpackages="$pkgname-lang $pkgname-cli:cli"
source="
	https://github.com/flxzt/rnote/archive/refs/tags/v$pkgver/rnote-$pkgver.tar.gz
	update_abseil.patch
	fix-pkgconfig.patch
"
options="net"

export CARGO_PROFILE_RELEASE_LTO=thin

build() {
	abuild-meson \
		. output

	meson configure -Dcli=true output
	meson compile -C output
}
check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

cli() {
	pkgdesc="Convert files to and from .rnote"

	amove usr/bin/rnote_cli
}

sha512sums="
4c8ed2f535fa280975622b95d1e6267a00fdf74d7cb78b98ac867f6629fb8175ae34d9c0b00d8aa7fb4c22d3a1453f8afeed7a2233520b609ab291392c12e8e6  rnote-0.7.1.tar.gz
d0322852b9ba51d45fced91460b83c3a8d35242ef725aa7bacaf5b37cc6ed00b05b57643a9048d444e8da5a67178e85644b69fe5cf2c8c9b991c134a68f8871c  update_abseil.patch
ec3c4cfbe7743969a5301bb9d6f95fa209905028f4dbc14b930802842089db8b9643a924b8d97a935cd10e380c5240e886cab10661e1043019944b6705baf379  fix-pkgconfig.patch
"
