# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=shadowsocks-rust
pkgver=1.15.4
pkgrel=2
pkgdesc="Rust port of shadowsocks"
url="https://github.com/shadowsocks/shadowsocks-rust"
arch="all"
license="MIT"
makedepends="
	cargo
	mimalloc2-dev
	openssl-dev
	"
subpackages="
	$pkgname-sslocal
	$pkgname-ssmanager
	$pkgname-ssserver
	$pkgname-ssservice
	$pkgname-ssurl
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/shadowsocks/shadowsocks-rust/archive/refs/tags/v$pkgver.tar.gz
	cargo-update-libc-lfs64.patch
	"
options="net !check" # fail for some reason

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		mimalloc = { rustc-link-lib = ["mimalloc"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	local features="
		aead-cipher-2022
		local-http-native-tls
		local-redir
		local-socks4
		local-tunnel
		logging
		manager
		mimalloc
		multi-threaded
		server
		service
		utility
		"
	case "$CARCH" in
	ppc64le|s390x|riscv64)
		;;
	*)
		# ioctl-sys
		features="$features local-tun"
		;;
	esac
	cargo build --release \
		--frozen \
		--bins \
		--no-default-features \
		--features="$(echo $features | tr -s " " ",")"
}

check() {
	cargo test --frozen
}

package() {
	depends="
		$pkgname-sslocal=$pkgver-r$pkgrel
		$pkgname-ssmanager=$pkgver-r$pkgrel
		$pkgname-ssserver=$pkgver-r$pkgrel
		$pkgname-ssservice=$pkgver-r$pkgrel
		$pkgname-ssurl=$pkgver-r$pkgrel
		"
	cd target/release
	install -Dm755 -t "$pkgdir"/usr/bin/ \
		sslocal \
		ssmanager \
		ssserver \
		ssservice \
		ssurl
}

sslocal() {
	pkgdesc="$pkgdesc (sslocal binary)"

	amove usr/bin/sslocal
}

ssmanager() {
	pkgdesc="$pkgdesc (ssmanager binary)"

	amove usr/bin/ssmanager
}

ssserver() {
	pkgdesc="$pkgdesc (ssserver binary)"

	amove usr/bin/ssserver
}

ssservice() {
	pkgdesc="$pkgdesc (ssservice binary)"

	amove usr/bin/ssservice
}

ssurl() {
	pkgdesc="$pkgdesc (ssurl binary)"

	amove usr/bin/ssurl
}

sha512sums="
fe61b11ab5e77212f88d4f58dc0a990f5805054472417e488132c7d1d825c0dcd7cc5efb4de65d39bd7d9ec4606e60704d1f35d7a1be59d4ca63b1e9e9dac5a4  shadowsocks-rust-1.15.4.tar.gz
aeba6b379e03ce3c2ce6e9d32ec9881d5cf91ce3bbea8f71b718c054fe738f2c388d17e8e87243a9bb2f24ce4787865679804a0312f88a36a1f3f00952645164  cargo-update-libc-lfs64.patch
"
