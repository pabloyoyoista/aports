# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=grocy
pkgver=4.0.1
pkgrel=0
pkgdesc="web-based self-hosted groceries & household management solution for your home"
url="https://grocy.info/"
license="MIT"
arch="noarch"
makedepends="composer yarn php8-dev php8-sqlite3 php8-gd php8-intl"
depends="php8 php8-fpm php8-sqlite3 php8-intl php8-gd php8-fileinfo php8-pdo_sqlite php8-ctype php8-mbstring php8-iconv php8-tokenizer sqlite"
source="$pkgname-$pkgver.zip::https://github.com/grocy/grocy/releases/download/v$pkgver/grocy_$pkgver.zip
	$pkgname-$pkgver.tar.gz::https://github.com/grocy/grocy/archive/refs/tags/v$pkgver.tar.gz
	grocy_nginx.conf
	grocy_nginx_fpm.conf"
options="!check" #no checks defined
subpackages="$pkgname-nginx:_nginx"

unpack() {
	#Web application is pre-packaged
	#Default unpack dumps this directly into the srcdir
	unzip $srcdir/$pkgname-$pkgver.zip -d $srcdir/$pkgname-$pkgver

	#But the web application lacks yarn & composer files to build with
	#so we pull and extract both
	mkdir $srcdir/source
	tar -xzvf $srcdir/$pkgname-$pkgver.tar.gz -C $srcdir/source
}

prepare() {
	default_prepare

	#Pull yarn & composer files from tarball, and insert into webapp directory
	for f in yarn.lock composer.lock composer.json composer.lock package.json; do
		cp $srcdir/source/$pkgname-$pkgver/$f $srcdir/$pkgname-$pkgver/
	done
}

build() {
	php8 -n -dextension=gd -dextension=intl /usr/bin/composer install --no-interaction --no-dev --optimize-autoloader
	php8 /usr/bin/composer clear-cache

	yarn install --modules-folder public/node_modules --production
	yarn cache clean
}

package() {
	_instdir="$pkgdir"/usr/share/webapps/grocy
	mkdir -p "$_instdir" "$pkgdir"/etc/webapps/grocy "$pkgdir"/var/lib/webapps

	cp -r "$builddir"/* "$_instdir"
	mv "$pkgdir"/usr/share/webapps/grocy/data "$pkgdir"/var/lib/webapps/grocy

	ln -s /var/lib/webapps/grocy "$pkgdir"/usr/share/webapps/grocy/data
	ln -s /etc/webapps/grocy/config.php "$pkgdir"/var/lib/webapps/grocy/config.php

	mv "$builddir"/config-dist.php "$pkgdir"/etc/webapps/grocy/config.php

	chown -R root:www-data "$pkgdir"/usr/share/webapps/grocy
	chown -R root:www-data "$pkgdir"/var/lib/webapps/grocy
}

_nginx() {
	pkgdesc="Nginx configuration for Grocy"
	depends="nginx !grocy-lighttpd"
	install="$subpkgname".pre-install

	install -d "$subpkgdir"/etc/nginx/sites-available
	install -d "$subpkgdir"/etc/php8/conf-available
	install -Dm0644 "$srcdir"/grocy_nginx.conf -t "$subpkgdir"/etc/nginx/sites-available
	install -Dm0644 "$srcdir"/grocy_nginx_fpm.conf -t "$subpkgdir"/etc/php8/conf-available
}

sha512sums="
c2fd4d9171bca8fa6bcf45664c5510931990fb1cb9c1007f6e9bb304f6da26741c15256cdaf20bd38a84aa517a28775f6e4323b65064280eebf32dbba128d73b  grocy-4.0.1.zip
4e893130ea95f92d7da3e0ffa0ed99858e294c56033fbd09adeb1dbae4e9add113cb863a2a3c56368a8768f12b2ff2190adaa8b91c5a7093eb49a2ef36f7e482  grocy-4.0.1.tar.gz
20f698a7b634ef6390f31275f2c0f8ca645e626521cb252c5c248c33616bd744ec0270f62bd7ffb3b56220dc37829ec8cc2692789ea1efffad8ba098e4c5caae  grocy_nginx.conf
707cb5c9837d75f6506b390207e544456d75fc2851791060261286d4cfa9534a785b84f26769d8d706e045a0f0cd1a0f3a30dcd3468b7c77ab237492580dc151  grocy_nginx_fpm.conf
"
